#jodb
Hello,developers.Welcome to our world!
Jodb（Java-Object-Database）is a light and easy-using ORM framework for web developing or any program needs to interact with SQL database.If you are one of these developers,it will be perfect because we have done all the basic and redundant work and now the only thing left there for you is to focus on your business logic.With the great features of Jodb,you don't need to care about what kind of SQL database server you are doing with.No matter it's mysql,oracle,postgres,sqlite,sqlServer or some other SQL servers,just do the same in Jodb.To learn more about this software,go straight to the demo and the source code.

Jodb（Java-Object-Database）是一个简单易用的ORM框架，通过它你可以不必关心自己所使用的是哪种类型的数据库，不必精通各种数据库的方言，在Jodb上，所有常用的数据库操作都是一样的。通过Jodb，你可以像操作一个简单的Java对象一样操作数据库。更多功能和特性，请看demo和源码。
